#pragma once
#include <cstdlib>  //Definition of NULL
//#include <exception>
#include <stdexcept>

using namespace std;



class MyStack
{
public:
	// Default constructor
	MyStack();
	MyStack(int);
	MyStack(const MyStack & st);

	// Destructor
	~MyStack();
	
	// Inserts and int on the top of the stack
	// PARAM: x is the value to be inserted
	void push(int x);
	
	// Removes and returns the item at the top of the stack
	// Throws a runtime_error if the stack is empty
	int pop();

	// Returns the item at the top of the stack
	// Throws a runtime_error if the stack is empty
	int peek() const;

	// Returns the number of items in the stack
	int size() const;

	// Overloaded operators
	MyStack & operator=(const MyStack & st);

private:

	void copyStack(const MyStack & st);
	int n;

	// dynamic Array Implementation
	int* arr;
	int top;

	//// Linked List Implementation
	//// Node class - defined inside the Stack class
	//class Node{
	//public:
	//	int data;
	//	Node* next;

	//	// Constructor
	//	Node(int x, Node* nd) : data(x), next(nd) {};
	//};
	//
	//Node* top;
	//void deleteStack();
	
	
	

};


