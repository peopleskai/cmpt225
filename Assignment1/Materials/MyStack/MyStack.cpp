#include "MyStack.h"
// There are two versions of this implementation
// One should always be commented out
// This is for illustration only

// Dynamic Array Version
//----------------------
// Default constructor - makes a stack of size 2
MyStack::MyStack()
{
	n = 2;
	arr = new int[n]; //dynamic array
	top = 0;
}

MyStack::MyStack(int sz)
{
	n = sz;
	arr = new int[n]; //dynamic array
	top = 0;
}

// Copy constructor
MyStack::MyStack(const MyStack & st)
{
	copyStack(st);
}

// Destructor
MyStack::~MyStack()
{
	delete[] arr; //deallocate dynamic memory
}

// Inserts an int on the top of the stack
// PARAM: x is the value to be inserted
void MyStack::push(int x)
{
	// Array is full
	if(top == n){
		// Make a new array of twice the size
		n = 2 * n;
		int* temp = arr;
		arr = new int[n];
		// Copy over the contents
		for (int i = 0; i < top; i++){
			arr[i] = temp[i];
		}
		// Deallocate memory for old array
		delete[] temp;
	}
	arr[top] = x;
	top++;
}

// Removes and returns the item at the top of the stack
// Throws a runtime_error if the stack is empty
int MyStack::pop()
{
	// Check to see if stack is empty
	if(top == 0){
		throw runtime_error("stack is empty");
	}
	top--;
	return arr[top];
}

// Returns the item at the top of the stack
// Throws a runtime_error if the stack is empty
int MyStack::peek() const
{
	// Check to see if stack is empty
	if(top == 0){
		throw runtime_error("stack is empty");
	}
	return arr[top - 1];
}

// Returns the number of items in the stack
// Not the size of the underlying array
int MyStack::size() const
{
	return top;
}

// Overloaded assignment operator - required because the class
// allocates space in main memory
// must work correctly for:
// st2 = st1 --> general case, including when st1 is empty
// st2 = st2 --> don't do anything in this case!
// st3 = st2 = st1 --> must return a MyStack
MyStack & MyStack::operator=(const MyStack & st)
{
	// Only copy the stack if it is a different object
	// The this pointer is the address of the calling object
	// i.e. if(calling object is not the parameter)
	if(this != &st){
		delete[] arr; //deallocate dynamic memory
		copyStack(st); //build new stack
	}
	return *this; //dereference pointer
}

// Copies the parameter into the calling object - makes
// a deep copy.  Helper method for the copy constructor
// and assignment operator.
void MyStack::copyStack(const MyStack & st)
{
	top = st.top;
	n = st.n;

	arr = new int[n];

	// Copy the contents of the array
	for(int i=0; i < top; i++){
		arr[i] = st.arr[i];
	}
}

///////////////////////////////////////////////
// Linked List Implementation

//// Default constructor - makes an empty stack
//MyStack::MyStack()
//{
//	top = NULL;
//	n = 0;
//}
//
//// Constructor - required for compliance with array version
//MyStack::MyStack(int sz)
//{
//	top = NULL;
//	n = 0;
//}
//
//// Copy constructor - required since the class allocates
//// space in dynamic memory and must therefore make a deep
//// copy
//// Note the parameter is a constant reference parameter
//MyStack::MyStack(const MyStack & st)
//{
//	copyStack(st); //builds new stack
//}
//
//// Destructor
//MyStack::~MyStack()
//{
//	deleteStack();
//	// DEBUG
//	// cout << "calling destructor" << endl;
//	// END dEBUG
//}
//
//// Inserts and int on the top of the stack
//// PARAM: x is the value to be inserted
//void MyStack::push(int x)
//{
//	Node* temp = new Node(x, top);
//	top = temp;
//	n++;
//}
//
//// Removes and returns the item at the top of the stack
//// Throws a runtime_error if the stack is empty
//int MyStack::pop()
//{
//	// Check to see if stack is empty
//	if(top == NULL){
//		throw runtime_error("stack is empty");
//	}
//	Node* temp = top; //save to delete
//	n--;
//	int result = temp->data; //save to return
//	top = top->next;
//	delete temp;
//	return result;	
//}
//
//// Returns the item at the top of the stack
//// Throws a runtime_error if the stack is empty
//int MyStack::peek() const
//{
//	if(top == NULL){
//		throw runtime_error("stack is empty");
//	}
//	return top->data;
//}
//
//// Returns the number of items in the stack
//// Not the size of the underlying array
//int MyStack::size() const
//{
//	return n;
//}
//
//// Overloaded assignment operator - required because the class
//// allocates space in main memory
//// must work correctly for:
//// st2 = st1 --> general case, including when st1 is empty
//// st2 = st2 --> don't do anything in this case!
//// st3 = st2 = st1 --> must return a MyStack
//MyStack & MyStack::operator=(const MyStack & st)
//{
//	// Only copy the stack if it is a different object
//	// The this pointer is the address of the calling object
//	// i.e. if(calling object is not the parameter)
//	if(this != &st){
//		deleteStack(); //deallocate dynamic memory
//		copyStack(st); //build new stack
//	}
//	return *this; //dereference pointer
//}
//
//// Deletes all the nodes in the stack.  Helper method for destructor
//// and assignment operator.
//void MyStack::deleteStack()
//{	
//	Node* temp = top;
//	// DEBUG
//	// cout << "In deleteStack" << endl;
//	// END DEBUG
//
//	// Iterate through list deleting Nodes
//	while(temp != NULL){
//		temp = top->next;
//		// DEBUG
//		// cout << "Deleting " << top->data << endl;
//		// END DEBUG
//		delete top;
//		top = temp;
//	}
//	top = NULL;
//}
//
//// Copies the parameter into the calling object - makes
//// a deep copy.  Helper method for the copy constructor
//// and assignment operator.
//void MyStack::copyStack(const MyStack & st)
//{
//	top = NULL;
//	n = st.n;
//	// Only make a copy if st is non-empty
//	if(st.top != NULL){
//		// Make a copy of the parameter
//		Node* original = st.top;
//		Node* copy;
//		// First copy the top
//		copy = new Node(original->data, NULL);
//		top = copy;
//		original = original->next;
//
//		// Now copy the rest of the stack
//		while(original != NULL){
//			copy->next = new Node(original->data, NULL);
//			copy = copy->next;
//			original = original->next;
//		}
//	}
//}

