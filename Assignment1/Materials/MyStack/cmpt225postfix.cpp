// cmpt225postfix.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>
#include <stdlib.h> 
#include "mystack.h"

using namespace std;

// Forward Declarations
string getNext(string input, int & pos);
int postFixCalculator(string exp);
void postFixTest(string exp);
void postFixExample();
void copyConstructorTest();
void assignmentOperatorTest();
void removePrintStack(MyStack st);

int main()
{
	postFixExample();
	copyConstructorTest();
	assignmentOperatorTest();

	
	cout << endl << endl;
	return 0;
}

// Parses a RPN string, returning the next operand or operator from
// the given position
// PRE: input is in the correct format
// PARAM: input - the RPN string
//        pos - the position to start parsing from
// Format: the string should contain a series of integer operands and 
//         the +, - * and / operators separated by spaces
string getNext(string input, int & pos){
	string result = "";
	int nextSpace = 0;
	// Ignore current space (given by pos)
	if(pos > 0){
		pos++;
	}
	if (pos < input.size()){
		// Find next space
		nextSpace = input.find(" ", pos+1);
		// DEBUG
		// cout << "nextSpace = " << nextSpace << endl;
		// END DEBUG
		result = input.substr(pos, nextSpace - pos);
		pos = nextSpace;
	}
	return result;
}

// Calculates and returns the result of a RPN expression
// PRE: input is in the correct format
// PARAM: exp - the RPN string
int postFixCalculator(string exp){
	// Initialize variables
	int pos = 0; // For parsing exp
	string next = ""; // Next operand or operator
	MyStack st(exp.size());
	int leftOp = 0;
	int rightOp = 0;

	// Process each operator or operand
	while (pos != -1){
		next = getNext(exp, pos);
		// DEBUG
		// cout << "next op: " << next << endl;
		// END DEBUG

		// Push operand onto stack
		if(next != "+" && next != "-" && next != "/" && next != "*"){
			st.push(atoi(next.c_str()));
		}
		// Or process each operator
		else{
			rightOp = st.pop();
			leftOp = st.pop();
			// DEBUG
			cout << leftOp << " " << next << " " << rightOp << endl;
			// END DEBUG
			if(next == "+"){ // +
				st.push(leftOp + rightOp);
			} else if(next == "-"){ // -
				st.push(leftOp - rightOp);
			} else if(next == "*"){ // *
				st.push(leftOp * rightOp);
			} else if(next == "/"){ // /
				st.push(leftOp / rightOp);
			} 
		}
	}

	// Return result
	return st.pop();
}

// Test function for the postFixCalculator function
void postFixTest(string exp)
{
	int result = postFixCalculator(exp);
	cout << exp << " = " << result;
}

// Calls postFixTest three times
void postFixExample()
{
	cout << "POSTFIX TEST" << endl << endl;
	postFixTest("5 1 2 + 4 * + 3 -");
	cout << endl << endl;
	postFixTest("9 7 + 5 3 - /");
	cout << endl << endl;
	postFixTest("3 5 2 * 17 7 - + * 11 3 * -");
	cout << endl << "END POSTFIX TEST" << endl << endl;
}

// Quick and dirty copy construcotr / assignment operator test
void copyConstructorTest()
{
	cout << "CONSTRUCTOR TEST" << endl << endl;
	MyStack st1;

	st1.push(1);
	st1.push(2);
	st1.push(3);

	MyStack st2(st1); //call copy constructor

	// Empty st1, shoudn't do anything to st2
	cout << "Empty st1" << endl;
	while(st1.size() > 0){
		cout << st1.pop() << endl;
	}
	
	// Call removePrintStack to print st2, and to
	// use copy constructor again
	cout << endl << "Pass st2 to removePrintStack (by value)" << endl;
	removePrintStack(st2);

	// Empty st2
	cout << endl << "Empty st2" << endl;
	while(st2.size() > 0){
		cout << st2.pop() << endl;
	}

	// Tests error handling by removing from an empty stack
	cout << endl << "Call pop on empty st1" << endl;
	try{		
		cout << st1.pop() << endl;
	}catch(runtime_error e){
		cout << e.what() << endl;
	}

	cout << endl << "END CONSTRUCTOR TEST" << endl << endl;
}

// Quick and dirty copy construcotr / assignment operator test
void assignmentOperatorTest()
{
	cout << "ASSIGNMENT TEST" << endl << endl;

	MyStack st1;
	MyStack st2;
	MyStack st3;

	cout << "Insert 1 -3 in st1, 100 - 102 in st2" << endl;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st2.push(100);
	st2.push(101);
	st2.push(102);

	cout << endl << "st 3 = st2 = st1" << endl;
	st3 = st2 = st1; //tests return value of op=
	
	// Empty st1, shoudn't do anything to st2 or st3
	cout << endl << "Empty st1" << endl;
	while(st1.size() > 0){
		cout << st1.pop() << endl;
	}

	// Empty st2
	cout << endl << "Empty st2" << endl;
	while(st2.size() > 0){
		cout << st2.pop() << endl;
	}
	
	cout << endl << "st 3 = st3 - shoudn't destroy stack!" << endl;
	st3 = st3; //tests op= doesn't fail on self-assignment
	// Empty st3
	cout << endl << "Empty st3" << endl;
	while(st3.size() > 0){
		cout << st3.pop() << endl;
	}

	cout << endl << "END ASSIGNMENT TEST" << endl;
}

// Removes items from a copy of parameter (pass-by-value)
void removePrintStack(MyStack st)
{
	while(st.size() > 0){
		cout << st.pop() << endl;
	}
}



