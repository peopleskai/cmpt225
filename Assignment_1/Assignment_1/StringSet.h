#pragma once
#include <string>

using namespace std;
class StringSet
{
public:
	StringSet();
	StringSet(const StringSet &stringSet);
	~StringSet();

	bool insert(string String);
	bool remove(string String);

	int find(string String) const;
	int size() const;

	StringSet unions(const StringSet &stringSet) const;
	StringSet intersection(const StringSet &stringSet) const;
	StringSet difference(const StringSet &stringSet) const;

private:
	string* underlyingArray;
	int top;

	void copyStringSet(const StringSet &stringSet);
	int n;
};