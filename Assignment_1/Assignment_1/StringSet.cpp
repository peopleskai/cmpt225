#include <StringSet.h>

StringSet::StringSet(){
	n = 4;
	underlyingArray = new string[n];
	top = 0;
}

StringSet::StringSet(const StringSet &stringSet){
	copyStringSet(stringSet);
}

StringSet::~StringSet(){
	delete[] underlyingArray;
}

bool StringSet::insert(string String){
	for (size_t i = 0; i < top; i++)
	{
		if (String == underlyingArray[i])
			return false;
	}

	if (n == top) {
		n = n*2;
		string* copyArray = new string[n];
		for (size_t i = 0; i < top; i++)
		{
			copyArray[i] = underlyingArray[i];
		}

		delete[] underlyingArray;
		underlyingArray = copyArray;
	}

	underlyingArray[top] = String;
	top++;

	return true;
}

bool StringSet::remove(string String){
	bool found = false;
	for (size_t i = 0; i < top; i++)
	{
		if (String == underlyingArray[i]) {
			underlyingArray[i] = underlyingArray[top-1];
			top--;
			found = true;
		}
	}
	if (!found)
		return false;
	
	return true;
}

int StringSet::find(string String) const{
	for (size_t i = 0; i < top; i++)
	{
		if (String == underlyingArray[i])
			return i;
	}

	return -1;
}

int StringSet::size() const{
	return top;
}

StringSet StringSet::unions(const StringSet &stringSet) const{
	StringSet newStringSet;

	for (size_t i = 0; i < top; i++)
	{
		newStringSet.insert(underlyingArray[i]);
	}
	for (size_t i = 0; i < stringSet.top; i++)
	{
		newStringSet.insert(stringSet.underlyingArray[i]);
	}

	return newStringSet;
}

StringSet StringSet::intersection(const StringSet &stringSet) const{
	StringSet newStringSet;

	for (size_t i = 0; i < top; i++)
	{
		for (size_t j = 0; j < stringSet.top; j++)
		{
			if (underlyingArray[i] == stringSet.underlyingArray[j])
				newStringSet.insert(underlyingArray[i]);
		}
	}

	return newStringSet;
}

StringSet StringSet::difference(const StringSet &stringSet) const{
	bool add;
	StringSet newStringSet;

	for (size_t i = 0; i < top; i++)
	{
		add = true;
		for (size_t j = 0; j < stringSet.top; j++)
		{
			if (underlyingArray[i] == stringSet.underlyingArray[j])
				add = false;
		}

		if (add)
			newStringSet.insert(underlyingArray[i]);
	}

	return newStringSet;
}

void StringSet::copyStringSet(const StringSet &stringSet){
	top = stringSet.top;
	n = stringSet.n;

	underlyingArray = new string[n];
	for (size_t i = 0; i < top; i++)
	{
		underlyingArray[i] = stringSet.underlyingArray[i];
	}
}