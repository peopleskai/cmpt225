#ifdef _RedBlackTree_
#include <stdexcept>
#include <RedBlackTree.h>

template <class T>
RedBlackTree<T>::RedBlackTree()
{
	root = NULL;
	sentinel = new Node<T>(true);
	treeSize = 0;
}

template <class T>
RedBlackTree<T>::RedBlackTree(const RedBlackTree<T> &copyTree)
{
		deepCopy(copyTree.root, copyTree.sentinel);
}

template <class T>
RedBlackTree<T> &RedBlackTree<T>::operator= (const RedBlackTree<T> &copyTree)
{
	if (this != &copyTree)
	{
		deleteTree();
		deepCopy(copyTree.root, copyTree.sentinel);
	}

	return *this;
}

template <class T>
RedBlackTree<T>::~RedBlackTree()
{
	deleteTree();
}

template <class T>
bool RedBlackTree<T>::insert(T value)
{
	Node<T>* newNode = new Node<T>(value);
	if (bstInsert(newNode))
	{
		newNode->isBlack = true;
		while (newNode != root && !newNode->parent->isBlack)
		{
			if (newNode->parent == newNode->parent->parent->left)
			{
				Node<T>* uncleNode = newNode->parent->parent->right;
				if (!uncleNode->isBlack)
				{
					newNode->parent->isBlack = true;
					uncleNode->isBlack = true;
					newNode->parent->parent->isBlack = false;
					newNode = newNode->parent->parent;
				}
				else
				{
					if (newNode == newNode->parent->right)
					{
						newNode = newNode->parent;
						leftRotate(newNode);
					}
					newNode->parent->isBlack = true;
					newNode->parent->parent->isBlack = false;
					rightRotate(newNode->parent->parent);
				}
			}
			else
			{
				Node<T>* uncleNode = newNode->parent->parent->left;
				if (!uncleNode->isBlack)
				{
					newNode->parent->isBlack = true;
					uncleNode->isBlack = true;
					newNode->parent->parent->isBlack = false;
					newNode = newNode->parent->parent;
				}
				else
				{
					if (newNode == newNode->parent->left)
					{
						newNode = newNode->parent;
						rightRotate(newNode);
					}
					newNode->parent->isBlack = true;
					newNode->parent->parent->isBlack = false;
					leftRotate(newNode);
				}
			}
		}
		return true;
	}
	else
	{
		delete[] newNode;
		return false;
	}
}

template <class T>
bool RedBlackTree<T>::remove(T value)
{
	return true;
}

template <class T>
bool RedBlackTree<T>::search(T value)
{
	return false;
}

template <class T>
T* RedBlackTree<T>::search(T first, T second, int &size)
{
	T* search;

	return search;
}

template <class T>
void RedBlackTree<T>::removeAll()
{
	deleteTree();
}

template <class T>
T* RedBlackTree<T>::dump(int &size)
{
	size = 0;
	T* copyArr = new T[treeSize];
	inOrder(root, copyArr, size);

	return copyArr;
}

template <class T>
int RedBlackTree<T>::size()
{
	return treeSize;
}

template <class T>
int RedBlackTree<T>::height()
{
	return treeSize;
}

template <class T>
Node<T>* RedBlackTree<T>::getRoot()
{
	return root;
}

template <class T>
void RedBlackTree<T>::deepCopy(const Node<T>* copyNode, const Node<T>* copySentinel)
{
	if(copyNode != copySentinel)
	{
		insert(copyNode->data);
		deepCopy(copyNode->left, copySentinel);
		deepCopy(copyNode->right, copySentinel);
	}
}

template <class T>
void RedBlackTree<T>::inOrder(const Node<T>* node, T* copyArr, int &size)
{
	if (node != sentinel)
	{
		inOrder(node->left, copyArr, size);
		copyArr[size++] = node->data;
		inOrder(node->right, copyArr, size);
	}
}

template <class T>
void RedBlackTree<T>::leftRotate(Node<T>* node)
{
	Node<T>* nodeRight = node->right;
	node->right = nodeRight->left;

	if (nodeRight->left != NULL)
		nodeRight->left->parent = node;

	nodeRight->parent = node->parent;

	if (node->parent == NULL)
		root = nodeRight;
	else if (node == node->parent->left)
		node->parent->left = nodeRight;
	else
		node->parent->right = nodeRight;

	nodeRight->left = node;
	node->parent = nodeRight;
}

template <class T>
void RedBlackTree<T>::rightRotate(Node<T>* node)
{
	Node<T>* nodeLeft = node->left;
	node->left = nodeLeft->right;

	if (nodeLeft->right != NULL)
		nodeLeft->right->parent = node;

	nodeLeft->parent = node->parent;

	if (node->parent == NULL)
		root = nodeLeft;
	else if (node == node->parent->left)
		node->parent->left = nodeLeft;
	else
		node->parent->right = nodeLeft;

	nodeLeft->right = node;
	node->parent = nodeLeft;
}

template <class T>
bool RedBlackTree<T>::bstInsert(Node<T>* newNode)
{
	Node<T>* parent = root;
	Node<T>* pos = root;
	bool isLeft = true;

	if (root == NULL)
		root = newNode;
	else
	{
		while (pos != sentinel)
		{
			parent = pos;
			if (newNode->data == parent->data)
				return false;
			else if (newNode->data < parent->data)
			{
				pos = parent->left;
				isLeft = true;
			}
			else
			{
				pos = parent->right;
				isLeft = false;
			}
		}

		if (isLeft)
			parent->left = newNode;
		else
			parent->right = newNode;
	}

	newNode->parent = parent;
	newNode->left = sentinel;
	//newNode->left->parent = newNode;
	newNode->right = sentinel;
	//newNode->right->parent = newNode;

	treeSize++;

	return true;
}

template <class T>
void RedBlackTree<T>::deleteTree()
{
	//deleteAll(root);
}

template <class T>
void RedBlackTree<T>::deleteAll(Node<T>* node)
{
	deleteAll(node->right);
	deleteAll(node->left);
	delete[] node;
}

#endif