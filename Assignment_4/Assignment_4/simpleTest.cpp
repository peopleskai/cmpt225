#include <iostream>
#include <string>
#include <stdexcept>
#include "RedBlackTree.h"

using namespace std;

// Function Prototypes
template <class T>
void printTree(RedBlackTree<T> tree);
template <class T>
void publicAttributesTest(Node<T>* nd);
void simpleTest();

void tonyTest();

int main()
{
	tonyTest();
	//simpleTest();

	cout << endl << endl;
	system("PAUSE");
	return 0;
}


void tonyTest()
{
	RedBlackTree<int> intTree1;
	RedBlackTree<int> intTree2;

	intTree1.insert(42);
	intTree1.insert(71);
	intTree1.insert(13);
	intTree1.insert(96);
	intTree1.insert(62);

	int n1;
	int* arr1 = intTree1.dump(n1);

	cout << "size = " << intTree1.size() << endl;
	cout << "root contains: " << intTree1.getRoot()->data << endl;
	for (int i = 0; i < n1; ++i){
		cout << arr1[i] << endl;
	}
	delete[] arr1;



	intTree2 = intTree1;
	int n2;
	int* arr2 = intTree2.dump(n2);

	cout << "size = " << intTree2.size() << endl;
	cout << "root contains: " << intTree2.getRoot()->data << endl;
	for (int i = 0; i < n2; ++i){
		cout << arr2[i] << endl;
	}
	delete[] arr2;
	
}


/*
void simpleTest()
{
	// Int Tree Tests     
	RedBlackTree<int> intTree1;
	RedBlackTree<int> intTree2;
	intTree1.insert(42);
	intTree1.insert(71);
	intTree1.insert(13);
	intTree1.insert(96);
	intTree1.insert(62);
	intTree2 = intTree1;
	intTree1.remove(13);
	intTree1.remove(96);
	printTree<int>(intTree2);
	cout << endl << "search" << endl << "------" << endl;
	cout << "42? " << intTree1.search(42) << endl;
	cout << "21? " << intTree1.search(21) << endl;

	cout << endl;

	// String Tree Tests
	RedBlackTree<string> stringTree1;
	stringTree1.insert("Aphrodite");
	stringTree1.insert("Artemis");
	stringTree1.insert("Athena");
	stringTree1.insert("Demeter");
	stringTree1.insert("Eris");
	stringTree1.insert("Hecate");
	RedBlackTree<string> stringTree2(stringTree1);
	stringTree1.remove("Artemis");
	stringTree1.remove("Athena");
	printTree<string>(stringTree2);
	cout << endl << "Athena ... Freya" << endl;
	cout << "----------------" << endl;
	int n = 0;
	string* arr = stringTree2.search("Athena", "Freya", n);
	for (int i = 0; i < n; i++){
		cout << arr[i] << endl;
	}
}
*/
template <class T>
void publicAttributesTest(Node<T>* nd)
{
	// Confirm that node attributes are accessible
	Node<T>* l = nd->left;
	Node<T>* r = nd->right;
	Node<T>* p = nd->parent;
	bool isBlack = nd->isBlack;
}

template <class T>
void printTree(RedBlackTree<T> tree)
{
	int n = 0;
	publicAttributesTest(tree.getRoot());
	T* arr = tree.dump(n);
	cout << "size = " << tree.size() << endl;
	cout << "height = " << tree.height() << endl;
	cout << "root contains: " << tree.getRoot()->data << endl;

	for (int i = 0; i < n; ++i){
		cout << arr[i] << endl;
	}
	delete[] arr;
}