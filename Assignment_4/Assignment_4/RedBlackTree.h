#ifndef _RedBlackTree_
#define _RedBlackTree_

template <class T>
class Node
{
public:
	T data;
	bool isBlack;
	Node* parent;
	Node* left;
	Node* right;

	Node(T data) : data(data), parent(NULL), left(NULL), right(NULL), isBlack(false) {};
	Node(bool isBlack) : data(NULL), parent(NULL), left(NULL), right(NULL), isBlack(isBlack) {};
};

template <class T>
class RedBlackTree
{
public:
	RedBlackTree();
	RedBlackTree(const RedBlackTree &copyTree);
	RedBlackTree &operator=(const RedBlackTree &copy);
	~RedBlackTree();

	bool insert(T value);
	bool remove(T value);
	bool search(T value);
	T* search(T first, T second, int &size);
	void removeAll();
	T* dump(int &size);
	int size();
	int height();
	Node<T>* getRoot();

private:
	int treeSize;
	Node<T>* root;
	Node<T>* sentinel;

	void deepCopy(const Node<T>* copyNode, const Node<T>* copySentinel);
	void inOrder(const Node<T>* node, T* copyArr, int &size);
	void leftRotate(Node<T>* node);
	void rightRotate(Node<T>* node);
	bool bstInsert(Node<T>* newNode);
	void deleteTree();
	void deleteAll(Node<T>* node);
};

#include "RedBlackTree.cpp"
#endif