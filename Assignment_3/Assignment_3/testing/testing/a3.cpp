#include <string>
#include <stdexcept>
#include <iostream>
#include <fstream>

using namespace std;

const int INSERTION = 0;
const int QUICK = 1;
const int MERGE = 2;
const int SHELL = 3;

template <class T>
T* readFile(string infile, int & n);
template <class T>
bool sorted(T arr[], int n);
template <class T>
bool contains(T arr1[], T arr2[], int n);
template <class T>
bool search(T arr[], int n, T target);
template <class T>
void sortTestResult(T arr[], int n, int sort);
void ssTest();
void isTest();
void qsTest();
void msTest();
void shsTest();

///////////////////////////////////////////////////////////////
//////////////////My Code Starts Here//////////////////////////
///////////////////////////////////////////////////////////////

// Initialization for inertion sort, quicksort, mergesort, shellsort functions
template <class T>
int insertionsort(T arr[], int size);
template <class T>
int quicksort(T arr[], int size);
template <class T>
int mergesort(T arr[], int size);
template <class T>
int shellsort(T arr[], int size);

// Initialization for partition function used in quicksort
template <class T>
int partitiion(T arr[], int low, int high, int &count);
// Initializtion of the recursive quicksort function
template<class T>
void quicksortdeep(T arr[], int low, int high, int &count);
// Initialization of the recursive mergesort function
template<class T>
void mergesortdeep(T arr[], int low, int high, int &count);
// Initialization of the merge function used in mergesort
template <class T>
void mergedeep(T arr[], int low, int pivot, int high, int &count);


int main()
{
	
	isTest();
	qsTest();
	msTest();
	shsTest();

	// Halt Program
	system("PAUSE");

	return 0;
}

// Insertion Sort Function
// PARAM: arr[] is the array to be sorted, size is the size of the array
// PRE: arr[] cannot be null and the size arguement must match the array's size
// POST: Arranges the array arguement and return the barometer count
template <class T>
int insertionsort(T arr[], int size)
{
	int count = 0;

	// Traverse through the array
	for (int i = 1; i < size; i++)
	{
		T temp = arr[i];
		int pos;
		
		// Traverse backwards through the array and rearrange array in the right order
		for (pos = i; pos > 0 && arr[pos - 1] > temp; pos--)
		{
			arr[pos] = arr[pos - 1];
			count++;
		}
		arr[pos] = temp;

		// Check to see if a comparison was made and barometer counter didn't increment
		if (pos == i)
			count++;
	}

	return count;
}

// Quicksort Function
// PARAM: arr[]: array to be sorted. size: the size of the array
// PRE: arr[] cannot be null and the size arguement must match the array's size
// POST: Arranges the array arguement and returns the barometer count
template <class T>
int quicksort(T arr[], int size)
{
	int count = 0;
	int low = 0;
	int high = size - 1;
	cout << endl;

	quicksortdeep(arr, low, high, count);

	return count;
}


// Mergesort Function
// PARAM: arr[]: array to be sorted. size: the size of the array
// PRE: arr[] cannot be null and the size arguement must match the array's size
// POST: Arranges the array arguement and returns the barometer count
template <class T>
int mergesort(T arr[], int size)
{
	int count = 0;
	
	mergesortdeep(arr, 0, size - 1, count);

	return count;
}

// Shellsort Function
// PARAM: arr[]: array to be sorted. size: the size of the array
// PRE: arr[] cannot be null and the size arguement must match the array's size
// POST: Arragnes the array arguement and returns the barometer count
template <class T>
int shellsort(T arr[], int size)
{
	int count = 0;
	int gap = size / 2;
	T temp;

	// Halves the gap by 2 until gap is 0
	for (gap; gap > 0; gap /= 2)
	{
		// Traverse through the array starting at first gap
		for (int i = gap; i < size; i++)
		{
			// Reverse Traverse the array with the specified gap to check for 
			// unordered elements and swap them
			int j = i - gap;
			while (j >= 0 && arr[j] > arr[j + gap])
			{
				temp = arr[j];
				arr[j] = arr[j + gap];
				arr[j + gap] = temp;

				count++;
				j -= gap;
			}
			count++;
		}
	}

	return count;
}

// Recursive function that partitions and arranges the array for Mergesort
// PARAM: arr[]: array to be sorted. low: start position to arrange. 
//			high: end position to arrange. count: barometer counter
// PRE: low must be the starting position of the array and high must be the
//		end position of the array
// POST: Arranges the specified part of the array and count the barometer operations
template <class T>
void mergesortdeep(T arr[], int low, int high, int &count)
{
	// Continue to call the recursive funtion mergesortdeep till we get to a partition of size 2
	if (low < high)
	{
		int pivot = (low + high) / 2;
		// Partition the array in half and arrange them
		mergesortdeep(arr, low, pivot, count);
		mergesortdeep(arr, pivot + 1, high, count);
		// Merge the Partitioned array
		mergedeep(arr, low, pivot, high, count);
	}
}

// Recursive function that merges the partitioned array for Mergesort
// PARAM: arr[]: array to be sorted. low: start position of the first partition
//			pivot: the end position for the first partition
//			high: start position for the second partition
//			count: counter for barometer operations
// PRE: low has to be the start of the start partition, pivot the end of the first partition
//		and high the end of the second partition
// POST: Merges the two partitions in an orderly manner
template <class T>
void mergedeep(T arr[], int low, int pivot, int high, int &count)
{
	int comparelow, comparehigh, pos;
	T* temp = new T[high - low +1];
	pos = 0;
	comparelow = low;
	comparehigh = pivot + 1;
	
	// Merges the two partition in an orderly manner
	while((comparelow <= pivot) && (comparehigh <= high))
	{
		if (arr[comparelow] <= arr[comparehigh])
			temp[pos++] = arr[comparelow++];
		else
			temp[pos++] = arr[comparehigh++];
	}

	// Fills in the last element
	if (comparelow > pivot)
		while (comparehigh <= high)
			temp[pos++] = arr[comparehigh++];
	else
		while (comparelow <= pivot)
			temp[pos++] = arr[comparelow++];
	
	// Update the array
	for (pos = 0; pos < (high - low + 1); pos++)
	{
		arr[pos + low] = temp[pos];
		count++;
	}
}

// Recursive function for Quicksort
// PARAM: arr[]: the array to be sorted. low: the start position
//			high: the end position. count: counter for the barometer operation
// PRE: low must the the start of the partition and high must be the end of the partition
// POST: Arranges the array and counts the barometer operation
template <class T>
void quicksortdeep(T arr[], int low, int high, int &count)
{
	// Continue to call the recursive funtion quicksortdeep till we get to a partition of size 2
	if (low < high)
	{
		// Apple the quicksort algorithm
		int pivot = partitiion(arr, low, high, count);

		// Call the recursive funtion quicksortdeep to arrange the first and second partition
		quicksortdeep(arr, low, pivot - 1, count);
		quicksortdeep(arr, pivot + 1, high, count);
	}
}

// Partition function for Quicksort
// PARAM: arr[]: the array to be sorted. low: the start position
//			high: the end position. count: counter for barometer operations
// PRE: low must be the start of the partition and high must be the end of the partition
// POST: Applies the quicksort algorithm to the specified part fo the array and counts
//			the barometer operations
template <class T>
int partitiion(T arr[], int low, int high, int &count)
{
	T pivot = arr[high];
	int pivotPos = high;
	int lowest = low;

	// Break when low pointer and high pointer meets
	while (low < high)
	{
		// Traverse the array till an element is smaller than the pivot
		while (arr[low] < pivot && high > low)
		{
			low++;
			count++;
		}

		// Reverse Traverse the array tell an element is bigger than the pivot
		if (high > low)
			high--;
		while (arr[high] > pivot && high > low)
		{
			high--;
			count++;
		}

		// Increment the barometer counter for cases when a comparison was made
		// but barometer counter wasn't incremented
		if (low == lowest)
			count++;
		if (high == low)
			count++;


		T tmp = arr[low];
		arr[low] = arr[high];
		arr[high] = tmp;
	}
	arr[pivotPos] = arr[low];
	arr[low] = pivot;

	return low;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////End of my Code//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// Tests the insertionsort function
void isTest()
{
	try{
		cout << "INSERTION SORT" << endl << "--------------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, INSERTION);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, INSERTION);
		string* arr3 = readFile<string>("test4.txt", n);
		sortTestResult(arr3, n, INSERTION);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, INSERTION);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the quicksort function
void qsTest()
{
	try{
		cout << "QUICKSORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, QUICK);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, QUICK);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, QUICK);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, QUICK);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the mergesort function
void msTest()
{
	try{
		cout << "MERGESORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, MERGE);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, MERGE);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, MERGE);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, MERGE);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the shellsort function
void shsTest()
{
	try{
		cout << "SHELL SORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, SHELL);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, SHELL);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, SHELL);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, SHELL);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests that array is correctly sorted
template <class T>
void sortTestResult(T arr[], int n, int sort)
{
	T* arrcopy = new T[n];
	for (int i = 0; i < n; i++){
		arrcopy[i] = arr[i];
	}

	cout << "n = " << n;
	if (sort == INSERTION){
		cout << ", comparisons = " << insertionsort(arr, n);
	}
	else if (sort == QUICK){
		cout << ", comparisons = " << quicksort(arr, n);
	}
	else if (sort == MERGE){
		cout << ", comparisons = " << mergesort(arr, n);
	}
	else if (sort == SHELL){
		cout << ", comparisons = " << shellsort(arr, n);
	}
	cout << ", values = " << contains(arr, arrcopy, n);
	cout << ", sorted = " << sorted(arr, n) << endl;

	delete[] arr;
	delete[] arrcopy;
}

// Opens a file and reads the contents into an array
// PARAM: infile = name of the file to be opened
//        n = the size of the result array
// PRE: the file contains values separated by white space
// POST: returns an array containing the contents of infile
template <class T>
T* readFile(string infile, int & n)
{
	T* result;
	T next;
	n = 0;

	ifstream ist(infile.c_str()); // open file
	// Check if file opened correctly
	if (ist.fail())
		throw runtime_error(infile + " not found");

	// Find file size
	while (ist >> next){
		n++;
	}

	// Read file into array
	ist.close();
	ist.open(infile.c_str());
	result = new T[n];
	for (int i = 0; i < n; ++i){
		ist >> result[i];
	}

	ist.close();

	return result;
}

// Checks to see if the input array is in ascending order
// PARAM: arr = name of the array
//        n = size of arr
// PRE:
// POST: returns true iff arr is in ascending order
template <class T>
bool sorted(T arr[], int n)
{
	// Check to see each element i <= element i+1
	for (int i = 0; i < n - 1; ++i){
		if (arr[i] > arr[i + 1]){
			return false;
		}
	}
	return true;
}

// Checks to see if the two arrays contain the same values
// PARAM: arr1, arr2 = name of the arrays
//        n = size of both arrays
// PRE: arr1 and arr2 are the same size, arr1 is sorted
// POST: returns true iff arr1 contains all values in arr2
template <class T>
bool contains(T arr1[], T arr2[], int n)
{
	// Check to see each element of arr2 is in arr1
	for (int i = 0; i < n; ++i){
		if (!search(arr1, n, arr2[i])){
			return false;
		}
	}
	return true;
}

// Checks to see if target value is in array
// PARAM: arr = array to be searched
//        n = size of array
//        target =  value to be searched for
// PRE: arr is sorted
// POST: returns true iff target is in arr
template <class T>
bool search(T arr[], int n, T target)
{
	int low = 0;
	int high = n - 1;
	int mid = 0;

	while (low <= high){
		mid = (low + high) / 2;
		if (arr[mid] == target){
			return true;
		}
		else if (target < arr[mid]){
			high = mid - 1;
		}
		else{
			low = mid + 1;
		}
	}
	return false;
}