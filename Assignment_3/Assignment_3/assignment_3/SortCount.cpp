#include <string>
#include <stdexcept>
#include <iostream>
#include <fstream>

using namespace std;

const int INSERTION = 0;
const int QUICK = 1;
const int MERGE = 2;
const int SHELL = 3;

template <class T>
T* readFile(string infile, int & n);
template <class T>
bool sorted(T arr[], int n);
template <class T>
bool contains(T arr1[], T arr2[], int n);
template <class T>
bool search(T arr[], int n, T target);
template <class T>
void sortTestResult(T arr[], int n, int sort);
void ssTest();
void isTest();
void qsTest();
void msTest();
void shsTest();


template <class T>
int insertionsort(T arr[], int count);
template <class T>
int quicksort(T arr[], int count);
template <class T>
int mergesort(T arr[], int count);
template <class T>
int shellsort(T arr[], int count);

template <class T>
int partitiion(T arr[], int low, int high, int &count);
template<class T>
void quicksortdeep(T arr[], int low, int high, int &count);
template<class T>
void mergesortdeep(T arr[], int low, int high, int &count);
template <class T>
void mergedeep(T arr[], int low, int pivot, int high, int &count);


int main()
{
	
	//isTest();
	qsTest();
	//msTest();
	//shsTest();

	// Halt Program
	system("PAUSE");

	return 0;
}


template <class T>
int insertionsort(T arr[], int size)
{
	int count = 0;
	cout << endl;
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << ", size: " << size << endl;

	for (int i = 1; i < size; i++)
	{
		T temp = arr[i];
		int pos;
		
		for (pos = i; pos > 0 && arr[pos - 1] > temp; pos--)
		{
			arr[pos] = arr[pos - 1];
			count++;
		}
		arr[pos] = temp;

		if (pos == i)
			count++;
	}
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;

	return count;
}

template <class T>
int quicksort(T arr[], int size)
{
	int count = 0;
	int low = 0;
	int high = size - 1;
	cout << endl;

	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << ", size: " << size << endl;

	quicksortdeep(arr, low, high, count);

	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;

	return count;
}

template <class T>
int mergesort(T arr[], int size)
{
	int count = 0;

	mergesortdeep(arr, 0, size - 1, count);

	return count;
}

template <class T>
int shellsort(T arr[], int size)
{
	int count = 0;
	int gap = size / 2;
	T temp;

	for (gap; gap > 0; gap /= 2)
	{
		for (int i = gap; i < size; i++)
		{
			int j = i - gap;
			while (j >= size && arr[j] > arr[j + gap])
			{
				temp = arr[j];
				arr[j] = arr[j + gap];
				arr[j + gap] = temp;

				j -= gap;
			}
		}
	}


	return count;
}

template <class T>
void mergesortdeep(T arr[], int low, int high, int &count)
{
	if (low < high)
	{
		int pivot = (low + high) / 2;
		mergesortdeep(arr, low, pivot, count);
		mergesortdeep(arr, pivot + 1, high, count);
		mergedeep(arr, low, pivot, high, count);
	}
}

template <class T>
void mergedeep(T arr[], int low, int pivot, int high, int &count)
{
	int comparelow, comparehigh, location;
	T* temp = new T[high - low + 1];
	comparelow = low;
	comparehigh = pivot + 1;
	
	while(comparelow <= pivot && comparehigh <= high)
	{
		if (arr[comparelow] <= arr[comparehigh])
			temp[location++] = arr[comparelow++];
		else
			temp[location++] = arr[comparehigh++];

		count++;
	}

	while (comparelow <= pivot)
		temp[location++] = arr[comparelow++];
	while (comparehigh <= high)
		temp[location++] = arr[comparehigh++];

	for (location = low; location < high; location++)
		arr[location] = temp[location];

	delete[] temp;
}

template <class T>
void quicksortdeep(T arr[], int low, int high, int &count)
{
	if (low < high)
	{
		int pivot = partitiion(arr, low, high, count);

		quicksortdeep(arr, low, pivot - 1, count);
		quicksortdeep(arr, pivot + 1, high, count);
	}
}

template <class T>
int partitiion(T arr[], int low, int high, int &count)
{
	T pivot = arr[high];
	int pivotPos = high;
	int dummy = low;

	cout << "count: " << count;

	while (low < high)
	{
		while (arr[low] < pivot)
		{
			low++;
			count++;
		}

		if (high > low)
			high--;
		while (arr[high] > pivot && high > low)
		{
			high--;
			count++;
		}
		if (low == dummy)
			count++;
		if (high == low)
			count++;


		T tmp = arr[low];
		arr[low] = arr[high];
		arr[low] = tmp;
	}
	arr[pivotPos] = arr[low];
	arr[low] = pivot;

	cout << ", count: " << count << endl;

	return low;
}



// Tests the insertionsort function
void isTest()
{
	try{
		cout << "INSERTION SORT" << endl << "--------------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, INSERTION);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, INSERTION);
		string* arr3 = readFile<string>("test4.txt", n);
		sortTestResult(arr3, n, INSERTION);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, INSERTION);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the quicksort function
void qsTest()
{
	try{
		cout << "QUICKSORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, QUICK);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, QUICK);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, QUICK);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, QUICK);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the mergesort function
void msTest()
{
	try{
		cout << "MERGESORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, MERGE);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, MERGE);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, MERGE);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, MERGE);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests the shellsort function
void shsTest()
{
	try{
		cout << "SHELL SORT" << endl << "---------" << endl;
		int n = 0;
		int* arr1 = readFile<int>("test1.txt", n);
		sortTestResult(arr1, n, SHELL);
		float* arr2 = readFile<float>("test2.txt", n);
		sortTestResult(arr2, n, SHELL);
		string* arr3 = readFile<string>("test3.txt", n);
		sortTestResult(arr3, n, SHELL);
		int* arr4 = readFile<int>("test5.txt", n);
		sortTestResult(arr4, n, SHELL);
		cout << endl;
	}
	catch (exception e){
		cout << e.what() << endl;
	}
}

// Tests that array is correctly sorted
template <class T>
void sortTestResult(T arr[], int n, int sort)
{
	T* arrcopy = new T[n];
	for (int i = 0; i < n; i++){
		arrcopy[i] = arr[i];
	}

	cout << "n = " << n;
	if (sort == INSERTION){
		cout << ", comparisons = " << insertionsort(arr, n);
	}
	else if (sort == QUICK){
		cout << ", comparisons = " << quicksort(arr, n);
	}
	else if (sort == MERGE){
		cout << ", comparisons = " << mergesort(arr, n);
	}
	else if (sort == SHELL){
		cout << ", comparisons = " << shellsort(arr, n);
	}
	cout << ", values = " << contains(arr, arrcopy, n);
	cout << ", sorted = " << sorted(arr, n) << endl;

	delete[] arr;
	delete[] arrcopy;
}

// Opens a file and reads the contents into an array
// PARAM: infile = name of the file to be opened
//        n = the size of the result array
// PRE: the file contains values separated by white space
// POST: returns an array containing the contents of infile
template <class T>
T* readFile(string infile, int & n)
{
	T* result;
	T next;
	n = 0;

	ifstream ist(infile.c_str()); // open file
	// Check if file opened correctly
	if (ist.fail())
		throw runtime_error(infile + " not found");

	// Find file size
	while (ist >> next){
		n++;
	}

	// Read file into array
	ist.close();
	ist.open(infile.c_str());
	result = new T[n];
	for (int i = 0; i < n; ++i){
		ist >> result[i];
	}

	ist.close();

	return result;
}

// Checks to see if the input array is in ascending order
// PARAM: arr = name of the array
//        n = size of arr
// PRE:
// POST: returns true iff arr is in ascending order
template <class T>
bool sorted(T arr[], int n)
{
	// Check to see each element i <= element i+1
	for (int i = 0; i < n - 1; ++i){
		if (arr[i] > arr[i + 1]){
			return false;
		}
	}
	return true;
}

// Checks to see if the two arrays contain the same values
// PARAM: arr1, arr2 = name of the arrays
//        n = size of both arrays
// PRE: arr1 and arr2 are the same size, arr1 is sorted
// POST: returns true iff arr1 contains all values in arr2
template <class T>
bool contains(T arr1[], T arr2[], int n)
{
	// Check to see each element of arr2 is in arr1
	for (int i = 0; i < n; ++i){
		if (!search(arr1, n, arr2[i])){
			return false;
		}
	}
	return true;
}

// Checks to see if target value is in array
// PARAM: arr = array to be searched
//        n = size of array
//        target =  value to be searched for
// PRE: arr is sorted
// POST: returns true iff target is in arr
template <class T>
bool search(T arr[], int n, T target)
{
	int low = 0;
	int high = n - 1;
	int mid = 0;

	while (low <= high){
		mid = (low + high) / 2;
		if (arr[mid] == target){
			return true;
		}
		else if (target < arr[mid]){
			high = mid - 1;
		}
		else{
			low = mid + 1;
		}
	}
	return false;
}