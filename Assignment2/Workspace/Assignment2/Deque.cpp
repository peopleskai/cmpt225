    #include<Deque.h>

// Default constructor
// PARAM: 
// PRE:
// POST: Creates an empty double ended queue
template <class T> Deque<T>::Deque(void) 
{
	front = NULL;
	back = NULL;
}

// Copy constructor
// PARAM: deque -> the double ended queue to be assigned to the calling object
// PRE:
// POST: Creates an object that is a copy of deque
template <class T> Deque<T>::Deque(const Deque<T> &deque)
{
	deepCopy(deque);
}

// Destructor
// PARAM:
// PRE:
// POST: Memory associated with the calling object are all de-allocated
template <class T> Deque<T>::~Deque()
{
	deleteDeque();
}

// Overloaded assignment operator
// In deque1 = deque2 assigns deque2 to deque1 by returning a copy of deque2
// PARAM: deque -> the double ended queue to be assigned to the calling object
// PRE:
// POST: Copies deque to calling object, returns calling object
template <class T> Deque<T> & Deque<T>::operator= (const Deque<T> &deque)
{
	if (this != &deque) {
		deleteDeque();
		deepCopy(deque);
	}

	return *this;
}

// Inserts user defined type variable to the front of the double ended queue
// PARAM: insert -> variable to be inserted to the front
// PRE:
// POST: variable insert is inserted in order into double ended queue
template <class T> void Deque<T>::insert_front(T insert) {
	if (front == NULL)	// First variable point both front and back to the root (the first variable)
	{
		front = new Node<T>(insert);
		back = front;
	}
	else
	{
		front = new Node<T>(insert, front);
	}
}

// Inserts user defined type variable to the back of the double ended queue
// PARAM: insert -> variable to be inserted to the back
// PRE:
// POST: variable insert is inserted in order into double ended queue
template <class T> void Deque<T>::insert_back(T insert) {
	if (back == NULL)	// First variable point both front and back to the root (the first variable)
	{
		back = new Node<T>(insert);
		front = back;
	}
	else
	{
		back->next = new Node<T>(insert);
		back = back->next;
	}
}

// Removes first variable from the double ended queue
// PARAM:
// PRE: Must use "try-catch" when calling this function to catch runtime execption
// POST: Removes the first variable from double ended queue 
//		 return the variable removed if queue not empty
//       otherwise throw runtime exception "Deque is empty"
template <class T> T Deque<T>::remove_front() {
	T value;

	if (front == NULL)	// the queue is empty
		throw runtime_error("Deque is empty");
	else	// remove the first element and point to the next one
	{
		Node<T>* temp = front;
		front = front->next;

		value = temp->data;
		temp = NULL;
	}

	return value;
}

// Removes last variable from the double ended queue
// PARAM:
// PRE: Must use "try-catch" when calling this function to catch runtime execption
// POST: Removes the last variable from double ended queue 
//		 if queue not empty return the variable removed
//       otherwise throw runtime exception "Deque is empty"
template <class T> T Deque<T>::remove_back() {
	T value;

	if (back == NULL)	// the queue is empty
	{
		throw runtime_error("Deque is empty");
	}
		
	// remove the last element and traverse queue to find the second last element and point the back pointer to it
	if (back != front)
	{
		Node<T>* temp = front;
		while (temp->next != back && back != front)
		{
			temp = temp->next;
		}

		value = back->data;
		back = NULL;
		back = temp;
	}
	else	// if the element to be removed is the last element, make front and back pointer null
	{
		value = back->data;
		back = NULL;
		front = NULL;
	}

	return value;
}

// Returns first variable from the double ended queue
// PARAM:
// PRE: Must use "try-catch" when calling this function to catch runtime execption
// POST: If queue not empty return the first variable from double ended queue
//       otherwise throw runtime exception "Deque is empty"
template <class T> T Deque<T>::peek_front() const
{
	if (front == NULL)
		throw runtime_error("Deque is empty");
	else
		return front->data;
}

// Returns last variable from the double ended queue
// PARAM:
// PRE: Must use "try-catch" when calling this function to catch runtime execption
// POST: If queue not empty return the last variable from double ended queue
//       otherwise throw runtime exception "Deque is empty"
template <class T> T Deque<T>::peek_back() const
{
	if (back == NULL)
		throw runtime_error("Deque is empty");
	else
		return back->data;
}

// Check if double ended queue is empty
// PARAM:
// PRE:
// POST: If queue empty return true
//       otherwise return false
template <class T> bool Deque<T>::empty() const
{
	if (front == NULL && back == NULL)
		return true;
	else
		return false;
}

// Check for the double ended queue's size
// PARAM:
// PRE:
// POST: returns the size of the queue as an integer
template <class T> int Deque<T>::size() const
{
	int size = 0;
	Node<T>* temp = front;

	// traverse queue to find the size of the queue
	while (temp != NULL)
	{
		size++;
		temp = temp->next;
	}

	return size;
}

// Makes a deep copy of a double ended queue
// PARAM: deque -> the double ended queue to copied
// PRE: Calling object is empty
// POST: Calling object contents are identical to deque
template <class T> void Deque<T>::deepCopy(const Deque<T> &deque)
{
	front = NULL;
	back = NULL;

	// traverse queue and copy the items one by one (original front = new back) while traversing
	if (deque.front != NULL)
	{
		Node<T>* original = deque.front;

		while (original != NULL)
		{
			insert_back(original->data);
			original = original->next;
		}
	}
}

// Removes all the items from the double ended queue and
// deallocates dynamic memory associated with nodes
// PARAM: 
// PRE:
// POST: double ended queue is empty
template <class T> void Deque<T>::deleteDeque()
{
	Node<T>* temp = NULL;

	// traverse queue and de-allocate each item while traversing until the last element
	while (front != back)
	{
		temp = front;
		front = front->next;
		delete[] temp;
	}

	// de-allocate the last item of the queue
	delete[] front;
}