#pragma once
#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;

template <class T>
class Node
{
public:
	// variable to hold data of the node
	T data;
	// pointer to hold address of next node
	Node* next;

	// constructor for node class with data input and next defaulted to null
	Node(T data) : data(data), next(NULL) {};
	// constructor for node class with data input and next input
	Node(T data, Node* next) : data(data), next(next) {};
};

template <class T>
class Deque
{
public:
	// constructor for deque class
	Deque();
	// copy constructor for deque class
	Deque(const Deque &deque);
	// destructor for deque class
	~Deque();

	// overload assignemtn operator
	Deque & operator= (const Deque &deque);

	void insert_front(T front);
	void insert_back(T back);
	T remove_front();
	T remove_back();
	T peek_front()const;
	T peek_back()const;
	bool empty()const;
	int size()const;

private:
	// variable to hold queues front address
	Node<T>* front;
	// variable to hold the queues end address
	Node<T>* back;

	void deepCopy(const Deque &deque);
	void deleteDeque();
};

