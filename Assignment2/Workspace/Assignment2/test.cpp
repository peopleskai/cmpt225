#include<Deque.h>
#include<Deque.cpp>

int main()
{
	Deque<string> test;

	test.insert_back("-1");
	test.insert_back("-2");
	test.insert_back("-3");
	test.insert_front("1");
	test.insert_front("2");
	test.insert_front("3");

	if (test.size() == 6)
		cout << "insert_front(): PASS\ninsert_back(): PASS\nsize() not null: PASS\n";
	else
		cout << "insert_front(): FAIL\ninsert_back(): FAIL\nsize() not null: FAIL\n";

	if (test.empty())
		cout << "empty() not null: FAIL\n";
	else
		cout << "empty() not null: PASS\n";

	if (test.peek_back() == "-3")
		cout << "peek_back() not null: PASS\n";
	else
		cout << "peek_back() not null: FAIL\n";

	if (test.size() == 6)
		cout << "size() not null: PASS\n";
	else
		cout << "size() not zero: FAIL\n";
	try{
		test.remove_back();
		test.remove_back();
		test.remove_front();
		test.remove_front();
	}
	catch (exception &e){
		cout << e.what() << endl;
	}

	if (test.peek_back() == "-1")
		cout << "remove_back() not pass root: PASS\n";
	else
		cout << "remove_back() not pass root: FAIL";

	if (test.peek_front() == "1")
		cout << "remove_front() not pass root: PASS\n";
	else
		cout << "remove_front() not pass root: FAIL\n";

	test.insert_back("-2");
	test.insert_back("-3");
	test.insert_front("2");
	test.insert_front("3");

	for (int i = 0; i < 5; i++)
	{
		test.remove_back();
	}

	if (test.peek_back() == "3")
		cout << "remove_back() pass root: PASS\n";
	else
		cout << "remove_back() pass root: FAIL\n";

	test.insert_back("2");
	test.insert_back("1");
	test.insert_back("-1");
	test.insert_back("-2");
	test.insert_back("-3");

	for (int i = 0; i < 5; i++)
	{
		test.remove_front();
	}

	if (test.peek_back() == "-3")
		cout << "remove_front() pass root: PASS\n";
	else
		cout << "remove_front() pass root: FAIL\n";

	test.remove_back();

	if (test.size() == 0)
		cout << "size() null: PASS\n";
	else
		cout << "size() null: FAIL\n";

	if (test.empty())
		cout << "empty() null: PASS\n";
	else
		cout << "empty() null: FAIL\n";

	try
	{
		test.remove_back();
	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}

	try
	{
		test.remove_front();
	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}

	Deque<int> dq1;
	cout << dq1.empty() << " - 1" << endl;

	dq1.insert_front(42);
	dq1.insert_back(216);

	cout << dq1.peek_front() << " - 42" << endl;
	cout << dq1.peek_back() << " - 216" << endl;
	cout << dq1.size() << " - 2" << endl;

	Deque<int> dq2(dq1);
	Deque<int> dq3;
	dq3 = dq1;

	cout << dq1.remove_front() << " - 42" << endl;
	cout << dq1.remove_back() << " - 216" << endl;

	cout << dq2.peek_front() << " - 42" << endl;
	cout << dq2.peek_back() << " - 216" << endl;

	cout << dq3.peek_front() << " - 42" << endl;
	cout << dq3.peek_back() << " - 216" << endl;

	// Halt Program
	system("PAUSE");
}