#pragma once
// John Edgar (johnwill@sfu.ca) - April 2011
// Class to represent a Node in the priority queue that drives Dijkstra's
// The node needs to record a vertex label, the cost from the start vertex
// to the vertex, and the predecessor vertex in the path

#include <string>
using namespace std;

class Node
{
public:
	// Constructor
	Node(string ident, int c, string previous) : id(ident), cost(c), 
		pre(previous) {};
	
	// Attributes
	string id;
	int cost;
	string pre;
	
	// Overloaded operators, to compare cost
	bool operator>(const Node & nd) const {return cost > nd.cost;}
	bool operator<(const Node & nd) const {return cost < nd.cost;}
	bool operator==(const Node & nd) const {return cost == nd.cost;}

};