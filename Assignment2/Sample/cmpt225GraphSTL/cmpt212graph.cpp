// cmpt212graph.cpp : Defines the entry point for the console application.
//

#include <iterator>
#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "Graph.h"

using namespace std;

// Forward Declarations

// Graph Functions
void constructorTest();
Graph createTestGraph();
void bfsTest();
void dfsTest();
void shortestPathTest();

// STL Lab Functions
vector<string> readFile(string fname);
set<string> makeStringSet(const vector<string> & v);
void notPets();
void wordTest();

int main()
{
	//notPets();
	//wordTest();

	//constructorTest();
	cout << endl << endl;
	bfsTest();
	cout << endl << endl;
	dfsTest();
	cout << endl << endl;
	shortestPathTest();

	cout << endl << endl;
	return 0;
}

void constructorTest(){
	cout << "CONSTRUCTOR TEST" << endl;
	
	string arr [] = {"v1", "v2", "v3", "v4"};
	const int SIZE = sizeof(arr) / sizeof(arr[0]);
	vector <string> keys;
	vector <string> test_keys;
	vector <string> test_edges;
	ostream_iterator <string> out (cout, "\n");
	
	// Copy string array to vector (for illustration)
	copy(arr, arr + SIZE, back_inserter(keys));
	
	Graph gr(keys); // create graph from vector

	// Check that graph has been created
	test_keys = gr.getVertices();
	copy(test_keys.begin(), test_keys.end(), out); //print
	
	// Insert a single edge
	gr.insertEdge("v1", "v2", 3);
	test_edges = gr.getEdges("v1"); //retrieve new edge from graph
	cout << endl << "v1 edges to:" << endl;
	copy(test_edges.begin(), test_edges.end(), out); //print
	cout << "-------------" << endl;
}

// Creates the graph used in class for illustration
Graph createTestGraph(){
	string arr [] = {"v1", "v2", "v3", "v4", "v5", "v6", "v7"};
	const int SIZE = sizeof(arr) / sizeof(arr[0]);
	vector <string> keys;

	copy(arr, arr + SIZE, back_inserter(keys));
	Graph gr(keys);

	// Insert Edges
	gr.insertEdge("v1", "v2", 2);
	gr.insertEdge("v1", "v4", 2);
	gr.insertEdge("v2", "v4", 3);
	gr.insertEdge("v2", "v5", 1);
	gr.insertEdge("v3", "v1", 4);
	gr.insertEdge("v3", "v6", 5);
	gr.insertEdge("v4", "v3", 2);
	gr.insertEdge("v4", "v5", 2);
	gr.insertEdge("v4", "v6", 8);
	gr.insertEdge("v4", "v7", 4);
	gr.insertEdge("v5", "v7", 1);
	gr.insertEdge("v7", "v6", 1);

	return gr;
}

// Creates the graph used in class for illustration
Graph createTestGraph2(){
	string arr [] = {"v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9"};
	const int SIZE = sizeof(arr) / sizeof(arr[0]);
	vector <string> keys;

	copy(arr, arr + SIZE, back_inserter(keys));
	Graph gr(keys);

	// Insert Edges	
	gr.insertEdge("v1", "v2", 1);
	gr.insertEdge("v1", "v6", 1);
	gr.insertEdge("v1", "v7", 1);
	gr.insertEdge("v2", "v3", 1);
	gr.insertEdge("v2", "v1", 1);
	gr.insertEdge("v3", "v4", 1);
	gr.insertEdge("v3", "v5", 1);
	gr.insertEdge("v3", "v1", 1);
	gr.insertEdge("v6", "v9", 1);
	gr.insertEdge("v7", "v8", 1);
	gr.insertEdge("v8", "v9", 1);
	gr.insertEdge("v8", "v7", 1);
	gr.insertEdge("v9", "v1", 1);
	gr.insertEdge("v9", "v8", 1);

	return gr;
}

// Simple test of breadth first search
void bfsTest(){
	cout << "BFS TEST" << endl;
	try{
		Graph gr = createTestGraph2();
		ostream_iterator <string> out (cout, ", ");
		vector<string> bfs_result = gr.bfs("v1");
		copy(bfs_result.begin(), bfs_result.end(), out);
		cout << endl << "-------------" << endl;
	}catch(invalid_argument ia){
		cerr << ia.what();
	}
}
// Simple test of depth first search
void dfsTest(){
	cout << "DFS TEST" << endl;
	try{
		Graph gr = createTestGraph2();
		ostream_iterator <string> out (cout, ", ");
		vector<string> dfs_result = gr.dfs("v1");
		copy(dfs_result.begin(), dfs_result.end(), out);
		cout << endl << "-------------" << endl;
	}catch(invalid_argument ia){
		cerr << ia.what();
	}
}


// Simple test of shortet path
void shortestPathTest(){
	cout << "SHORTEST PATH TEST" << endl;
	try{
		Graph gr = createTestGraph();
		map<string, string> path_result = gr.shortestPath("v1");
		vector<string> keys = gr.getVertices();

		// Output shortest path data
		vector<string>::iterator it_keys;
		for(it_keys = keys.begin(); it_keys != keys.end(); it_keys++){
			cout << "to " << *it_keys << " via ";
			cout << path_result.find(*it_keys)->second << endl;
		}
		cout << "-------------" << endl;
	}catch(invalid_argument ia){
		cerr << ia.what();
	}
}

// ------------------------------------------------------------------
void notPets(){
	// Read the mammals file
	set<string> mammals = makeStringSet(readFile("mammals.txt"));
	// Read the pets file
	set<string> pets = makeStringSet(readFile("pets.txt"));
	ostream_iterator<string, char> out (cout, "\n");
	//copy(mammals.begin(), mammals.end(), out);
	// Find and print the set difference
	set_difference(mammals.begin(), mammals.end(),
		pets.begin(), pets.end(), out);
}

void wordTest(){
	set<string> dict = makeStringSet(readFile("wordsEn.txt"));
	cout << dict.size() << " WORDS" << endl;

	string word1 = "antonym";
	cout << word1;
	set<string>::const_iterator start;
	start = dict.find(word1);
	//end

}

set<string> makeStringSet(const vector<string> & v){
	set<string> s(v.begin(), v.end());
	return s;
}

vector<string> readFile(string fname){
	// Create an input stream from a file
	ifstream in_file(fname.c_str());
	// Point an iterator at the file
	istream_iterator<string> start_stream(in_file);
	// Create an iterator that points to the end of a stream
	istream_iterator<string> end_stream;
	// Create the vector from the stream
    vector <string> v(start_stream, end_stream);
	return v;
}

