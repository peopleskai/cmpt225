// Implementation of the Graph class

#include "stdafx.h"
#include "Graph.h"

// NOTE ON MAPS
//----------------------------------------------------
// STL maps consist of <key, value> pairs where *pair* is an STL template.
// The pair template has two attributes, first and second, where, for a map
// *first* represents the key and *second* the value.
//
// Pairs can be created either "by hand":
// pair<string, int> pr = pair<string, int> ("bob", 13); 
// or by using the make_pair function which figures out the template types
// pair<string, int> pr = make_pair("bob", 13);
//
// Map iterators therefore point to *pair*s, so that the key or value has
// to be retrieved by accessing the first or second attribute of the pair.
// So, if it_map is a map iterator:
// string key = it_map->first;
// retrieves the first value of the pair, the key for that map pair
//
// END NOTE
//----------------------------------------------------

// Function to return the key in an adjacency list
string getKey(pair<string, map<string, int> > p) {return p.first;}
// Function to return the first element of a pair
string first(pair<string, int> p) {return p.first;}
// For use in alternative version of transform in constructor
pair<string, map<string, int> > make_vertex(string s){
	return make_pair(s, map<string, int> ());
}

const int INF = 1000; //Represents infinity for shortest path algorithm

// Default constructor
Graph::Graph(void)
{
}

// Constructor that creates a graph containing the vertex labels stored
// in v.  Note that since a map contains unique key values, any duplicates
// contained in v will be ignored
Graph::Graph(vector<string> v)
{
	// This is a demonstration of the STL transform function, and also
	// includes an example of bind2nd and prt_fun

	// The transform function takes four arguments which I'll describe
	// v.begin() - iterator to the start of v
	// v.begin() - iterator to one-past-the-end of v
	// inserter(adjList, adjList.begin()) - returns an insert iterator to
	//                                      the adjacency list
	// function - transform requires a function that is applied to each of
	// the values in the input container, v in this case.  We want to 
	// transform each string in v into a <string, map<string, int> > pair.
	// We can use make_pair to do this, but make_pair requires two
	// arguments, and we want to just give it the label and create an
	// empty map.  We can do this by using bind2nd to bind our default
	// value (the empty map) to make_pair's second argument.  Finally (!),
	// since make_pair is not a functor we need to turn it into one, which
	// is what ptr_fun is doing.
	// So the entire function consists of one line of C++ code, although
	// the explanation is about 20 lines long :)
	//transform(v.begin(), v.end(), inserter(adjList, adjList.begin()),
		//bind2nd(ptr_fun(make_pair<string, map<string, int> >), map<string, int> ()));

	// Additional note re VS 2010
	//---------------------------
	// Just to make this more fun the call to transform shown above doesn't
	// work in VS 2010.  This is because the 2010 version of make_pair is
	// overloaded with four (slightly) different versions. Unfortunately,
	// ptr_fun is unable to figure out which one to use, resulting in a
	// compiler error.  To solve this we can write our own transform
	// function and call it in transform.  I've shown this function at the 
	// start of the file (it's called make_vertex), we could then replace
	// the transfom shown above with the one below to be VS2010 compliant
	//
	transform(v.begin(), v.end(), 
	  	inserter(adjList, adjList.begin()), make_vertex);
}

Graph::~Graph(void) {}

// Inserts the vertex name in the graph, throws invalid_argument
// if a vertex with that label already exists in the graph
void Graph::insertVertex(string name)
{
	// The map insert method only inserts a pair if the value does not
	// already exist in the graph.  It returns a pair consisting of an
	// iterator to new, or existing pair, and a bool that specifies
	// whether or not the insertion succeeded
	if(!adjList.insert(make_pair(name, map<string, int> ())).second){
		throw invalid_argument("vertex already exists in graph");
	}
}

// Inserts an edge in the graph from *from* to *to* with *cost*, throws 
// invalid_argument if *from* or *to* are not in the graph
void Graph::insertEdge(string from, string to, int cost)
{
	// Create an iterator to the from vertex
	map<string, map<string, int> >::iterator it;
	it = adjList.find(from);
	// Check that from vertex is in the graph
	if(it == adjList.end()){
		throw invalid_argument("from vertex not in graph");
	}
	// Check that to vertex is in the graph
	if(adjList.find(to) == adjList.end()){
		throw invalid_argument("to vertex not in graph");
	}
	// Check that an edge does not already exist
	if(it->second.find(to) != it->second.end()){
		throw invalid_argument("edge already exists");
	}
	// Inserts the new edge in the from vertex' list of edges
	it->second.insert(pair<string, int> (to, cost) );
}

// Returns a vector of vertex labels of vertices adjacent to *from*,
// throws invalid_argument if *from* is not in the graph
vector<string> Graph::getEdges(string from) const
{
	vector<string> result;
	// Iterator to from vertex, it is a constant iterator since the
	// key or value should not change when retrieving the edges
	map<string, map<string, int> >::const_iterator it;
	it = adjList.find(from); // find from vertex

	// Check that from vertex is in the graph
	if(it == adjList.end()){
		throw invalid_argument("from vertex not in graph");
	}
	
	// Use transform to insert adjacent vertices in a vector
	// it->second is the second value in an adjacency list pair, that is
	// a map<string, int> representing edges, so it->second.begin() and 
	// it->second.end() are the start and end of this edge list
	//
	// The first function is defined at the head of this file and returns
	// the first value in a <string, int> pair, it is required because the
	// transform function needs a function argument
	transform(it->second.begin(), it->second.end(), 
		back_inserter(result), first);
	return result;
}

// Returns a vector of vertex labels of vertices that are NOT adjacent
// to *from*, throws invalid_argument if *from* is not in the graph
vector<string> Graph::getNonEdges(string from) const {
	vector<string> result;
	set<string> neighbours;
	set<string> all;
	// Iterator to from vertex, it is a constant iterator since the
	// key or value should not change when retrieving the edges
	map<string, map<string, int> >::const_iterator it_vertex;

	// Check that from vertex is in the graph
	it_vertex = adjList.find(from); // find from vertex
	if(it_vertex == adjList.end()){
		throw invalid_argument("from vertex not in graph");
	}
	
	// Insert all vertex labels in set
	transform(adjList.begin(), adjList.end(), 
		inserter(all, all.begin()), getKey);

	// Insert neighbours of from in set	
	transform(it_vertex->second.begin(), it_vertex->second.end(), 
		inserter(neighbours, neighbours.begin()), first);

	// Copy result to vector
	set_difference(all.begin(), all.end(), neighbours.begin(),
		neighbours.end(), back_inserter(result));
	
	return result;
}

// Returns a vector of vertex labels in the graph
vector<string> Graph::getVertices() const
{
	vector<string> result;
	// This is very similar to getEdges except that the function in
	// transform is different since it is returning the first value in 
	// a <string, map<string, int> > pair
	transform(adjList.begin(), adjList.end(), back_inserter(result), getKey);
	return result;
}

// Returns a vector of vertex labels of vertices that are connected to 
// *start*, the vertices appear in the vector in the same order that they 
// would be visited by a breadh first search, throws invalid_argument if
// *start* is not in the graph
//vector<string> Graph::bfs(string start) const
//{
//	// Note that the STL vector is a sequence, so guarantees that the
//	// order of elements will not arbitrarily change
//	// Containers
//	vector<string> result; 
//	queue<string> q; // to drive the bfs
//	set<string> visited; // to record which vertices have been visited
//
//	// Other variables
//	string current; // label of the current vertex
//	map<string, map<string, int> >::const_iterator it_vertex;
//	map<string, int>::const_iterator it_edge;
//
//	// Check that from vertex is in the graph
//	it_vertex = adjList.find(start);
//	if(it_vertex == adjList.end()){
//		throw invalid_argument("start vertex not in graph");
//	}
//	
//	// Initialization
//	q.push(start); // insert start vertex in q
//	visited.insert(start);
//	result.push_back(start);
//
//	// Perform the bfs from the start vertex
//	// The process is as follows:
//	//
//	// Until the queue is empty
//	// ---Remove vertex from the front of the queue
//	// ---For each adjacent vertex to the current
//	// ------If the vertex is not already visited
//	//---------- Visit the vertex and insert it in the queue
//	while(!q.empty()){
//		current = q.front();
//		q.pop(); // remove current vertex from queue
//		it_vertex = adjList.find(current); //get iterator to vertex
//
//		// Process each edge adjacent to current
//		for(it_edge = it_vertex->second.begin();
//			it_edge != it_vertex->second.end(); ++it_edge){
//			
//			// Check to see if the vertex has already been visited
//			if(visited.find(it_edge->first) == visited.end()){
//				// If not, visit it and insert it in the queue
//				q.push(it_edge->first);
//				visited.insert(it_edge->first);
//				result.push_back(it_edge->first);
//			}
//		}		
//	}
//	return result;
//}

// Returns a vector of vertex labels of vertices that are connected to 
// *start*, the vertices appear in the vector in the same order that they 
// would be visited by a breadth first search, throws invalid_argument if
// *start* is not in the graph
vector<string> Graph::bfs(string start) const
{
	// Note that the STL vector is a sequence, so guarantees that the
	// order of elements will not arbitrarily change
	// Containers
	vector<string> result; 
	queue<string> q; // to drive the bfs
	set<string> visited; // to record which vertices have been visited

	// Other variables
	string current; // label of the current vertex
	map<string, map<string, int> >::const_iterator it_vertex;
	map<string, int>::const_iterator it_edge;

	// Check that from vertex is in the graph
	it_vertex = adjList.find(start);
	if(it_vertex == adjList.end()){
		throw invalid_argument("start vertex not in graph");
	}
	
	// Initialization
	q.push(start); // insert start vertex in q
	visited.insert(start);
	result.push_back(start);

	// Perform the bfs from the start vertex
	// The process is as follows:
	//
	// Until the queue is empty
	// ---Remove vertex from the front of the queue
	// ---For each adjacent vertex to the current
	// ------If the vertex is not already visited
	//---------- Visit the vertex and insert it in the queue
	while(!q.empty()){
		current = q.front();
		//q.pop(); // remove current vertex from queue
		it_vertex = adjList.find(current); //get iterator to vertex
		bool found = false;

		// Get an un-visited adjacent vertex
		it_edge = it_vertex->second.begin();
		while(!found && it_edge != it_vertex->second.end()){
			
			// Check to see if the vertex has already been visited
			if(visited.find(it_edge->first) == visited.end()){
				// If not, visit it and insert it in the queue
				q.push(it_edge->first);
				visited.insert(it_edge->first);
				result.push_back(it_edge->first);
				found = true;
			}
			++it_edge;
		}
		if(!found){
			q.pop();
		}
	}
	return result;
}

// Returns a vector of vertex labels of vertices that are connected to 
// *start*, the vertices appear in the vector in the same order that they 
// would be visited by a depth first search, throws invalid_argument if
// *start* is not in the graph
vector<string> Graph::dfs(string start) const
{
	// Note that the STL vector is a sequence, so guarantees that the
	// order of elements will not arbitrarily change
	// Containers
	vector<string> result; 
	stack<string> st; // to drive the bfs
	set<string> visited; // to record which vertices have been visited

	// Other variables
	string current; // label of the current vertex
	map<string, map<string, int> >::const_iterator it_vertex;
	map<string, int>::const_iterator it_edge;

	// Check that from vertex is in the graph
	it_vertex = adjList.find(start);
	if(it_vertex == adjList.end()){
		throw invalid_argument("start vertex not in graph");
	}
	
	// Initialization
	st.push(start); // insert start vertex in st
	visited.insert(start);
	result.push_back(start);

	// Perform the bfs from the start vertex
	// The process is as follows:
	//
	// Until the queue is empty
	// ---Remove vertex from the front of the queue
	// ---For each adjacent vertex to the current
	// ------If the vertex is not already visited
	//---------- Visit the vertex and insert it in the queue
	while(!st.empty()){
		current = st.top();
		//st.pop(); // remove current vertex from queue
		it_vertex = adjList.find(current); //get iterator to vertex
		bool found = false;

		// Get an un-visited adjacent vertex
		it_edge = it_vertex->second.begin();
		while(!found && it_edge != it_vertex->second.end()){
			
			// Check to see if the vertex has already been visited
			if(visited.find(it_edge->first) == visited.end()){
				// If not, visit it and insert it in the queue
				st.push(it_edge->first);
				visited.insert(it_edge->first);
				result.push_back(it_edge->first);
				found = true;
			}
			++it_edge;
		}
		if(!found){
			st.pop();
		}
	}
	return result;
}

// Returns a vector of vertex labels of vertices that are connected to 
// *start*, the vertices appear in the vector in the same order that they 
// would be visited by a depth first search, throws invalid_argument if
// *start* is not in the graph


// Returns a map <name, pre> where *name* is a vertex label and *pre*
// is the vertex label of the vertex precesing *name* in the shortest
// path to it from *start*, throws invalid_argument if *start* is not
// in the graph
map<string, string> Graph::shortestPath(string start) const
{
	// Containers
	map<string, string> paths; // result
	priority_queue<Node, vector<Node>, greater<Node> > pq;
	map<string, int> costs; // least cost to vetex

	// Iterators
	map<string, map<string, int> >::const_iterator it_vertex;
	map<string, int>::const_iterator it_edge;
	map<string, int>::iterator it_cost;

	// Check that from vertex is in the graph
	it_vertex = adjList.find(start);
	if(it_vertex == adjList.end()){
		throw invalid_argument("start vertex not in graph");
	}

	// Initialization - Prepare priority queue and cost map - cost O(v)
	for(it_vertex = adjList.begin(); 
		it_vertex != adjList.end(); ++it_vertex){
		
		// Insert start vertex in priority queue and in costs
		if(it_vertex->first == start){
			pq.push(Node(start, 0, start));
			costs.insert(pair<string, int>(it_vertex->first, 0));
		}else{
			// Insert all other vertices in costs with cost of infinity
			costs.insert(pair<string, int>(it_vertex->first, INF));
		}
	}

	// Run Dijkstra's algorithm
	// The major difference with this STL version and the traditional
	// version is that costs to reach a vertex from the start are not
	// changed in the priority queue, instead they are changed in a 
	// costs map (if the new cost is less) and a new entry is inserted
	// in the priority queue with the lower cost
	while(!pq.empty()){
		// Remove item at root of prQ
		Node current = pq.top();
		pq.pop();
		it_vertex = adjList.find(current.id); //O(log n)

		// Check to see if vertex is NOT in paths map
		// Since, if it is in the paths map we have already found the
		// shortest path to it so the current node would represent a
		// longer path that simply needs to taken out of the queue
		if(paths.find(current.id) == paths.end()){
			// Process each edge from current, as in other methods note
			// that it_vertex->second is a map<string, int> of edges
			for(it_edge = it_vertex->second.begin();
				it_edge != it_vertex->second.end(); ++it_edge){

					// Find cost to vertex via edge and update if less
					// Here the components of this_path_cost are:
					// 1 - costs.find(current.id)->second, the cost from
					// the start to current vetex, and
					// 2 - it_edge->second, the cost from the current
					// vertex to its neighbour
					// So this_path_cost is the cost to a neigbour from the
					// start and via current
					int this_path_cost = 
						costs.find(current.id)->second + it_edge->second;
					// it_cost is an iterator to the cost pair for the 
					// same neighbour of current, i.e. the cost that is
					// to date the shortest path cost to this neighbour
					it_cost = costs.find(it_edge->first);
					
					// Update if necessary
					if(this_path_cost < it_cost->second){
						// Change the entry in the costs map, and
						// Insert a new entry in the priority queue
						it_cost->second = this_path_cost;
						pq.push(Node(it_cost->first, it_cost->second, 
							current.id));
					}
			}
			// Insert an entry in the result map
			paths.insert(pair<string, string>(current.id, current.pre));
		}
	}
	return paths;
}