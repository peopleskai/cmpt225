// John Edgar (johnwill@sfu.ca) - April 2011
// Class to represent a weighted, directed graph using STL containers

#pragma once

#include<map>
#include<set>
#include<queue>
#include<stack>
#include<string>
#include<vector>
#include<algorithm>
#include<iterator>
#include<utility>
#include<functional>
#include<stdexcept>
#include "Node.h"

using namespace std;


class Graph
{
public:
	// Constructors and Destructor

	// Creates an empty graph
	Graph(void);
	// PARAM: v is a vector of vertex labels
	// Creates a graph containing the vertexes in v, with no edges
	Graph(vector<string> v);
	~Graph(void);
	
	// PARAM: name is a vertex label
	// Inserts the vertex name in the graph, throws invalid_argument
	// if a vertex with that label already exists in the graph
	void insertVertex(string name) throw(invalid_argument);

	// PARAM: from and to are a vertex labels, cost is the weight of the
	//        edge from *from* to *to*
	// Inserts an edge in the graph from *from* to *to* with *cost*, 
	// throws invalid_argument if either the *from* or *to* vertices 
	// are not in the graph
	void insertEdge(string from, string to, int cost)throw(invalid_argument);
	
	// PARAM: from is a vertex label
	// Returns a vector of vertex labels of vertices adjacent to *from*,
	// throws invalid_argument if *from* is not in the graph
	vector<string> getEdges(string from) const throw(invalid_argument);

	// PARAM: from is a vertex label
	// Returns a vector of vertex labels of vertices that are NOT adjacent
	// to *from*, throws invalid_argument if *from* is not in the graph
	vector<string> getNonEdges(string from) const throw(invalid_argument);
	
	// Returns a vector of vertex labels in the graph
	vector<string> getVertices() const;
	
	// PARAM: start is a vertex label
	// Returns a vector of vertex labels of vertices that are connected
	// to *start*, the vertices appear in the vector in the same order
	// that they would be visited by a breadth first search, throws
	// invalid_argument if *start* is not in the graph
	vector<string> bfs(string start) const throw(invalid_argument);

	// PARAM: start is a vertex label
	// Returns a vector of vertex labels of vertices that are connected
	// to *start*, the vertices appear in the vector in the same order
	// that they would be visited by a depth first search, throws
	// invalid_argument if *start* is not in the graph
	vector<string> dfs(string start) const throw(invalid_argument);
	
	// PARAM: start is a vertex label
	// Returns a map <name, pre> where *name* is a vertex label and *pre*
	// is the vertex label of the vertex precesing *name* in the shortest
	// path to it from *start*, throws invalid_argument if *start* is not
	// in the graph
	map<string, string> shortestPath(string start) const throw(invalid_argument);

private:
	// Adjacency list representing the graph, the classic representation
	// of an adjacency list is an array of linked lists, where the linked
	// lists contain <vertex, cost> pairs.  Searching the linked lists for
	// a vertex is O(n)
	// This list consists of a map of maps where the inner map represents
	// the edges from its vertex key.  Finding keys in a map is O(log n)
	map<string, map<string, int> > adjList;
};
